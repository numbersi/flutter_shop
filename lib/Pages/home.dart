import 'dart:html';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}


class _HomePageState extends State<HomePage> {
   List banner = [{"image_src":"http://api-hmugo-web.itheima.net/pyg/banner1.png","open_type":"navigate","goods_id":129,"navigator_url":"/pages/goods_detail/main?goods_id=129"},{"image_src":"http://api-hmugo-web.itheima.net/pyg/banner2.png","open_type":"navigate","goods_id":395,"navigator_url":"/pages/goods_detail/main?goods_id=395"},{"image_src":"http://api-hmugo-web.itheima.net/pyg/banner3.png","open_type":"navigate","goods_id":38,"navigator_url":"/pages/goods_detail/main?goods_id=38"}];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: 180,
        child: Column(
          children: [
          Container(height:180,
            child:
          PageView.builder(
              itemCount: banner.length,
              itemBuilder: (_,i){
            return GestureDetector(child: Container(
              height: 180,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(40  ),
                  image: DecorationImage(image: NetworkImage(banner[i]['image_src']),fit:BoxFit.cover)
              ),
            ),onTap: (){
              print("onPointerUp");
            });
          }) ,)
        ],),
      ),
    );
  }
}

